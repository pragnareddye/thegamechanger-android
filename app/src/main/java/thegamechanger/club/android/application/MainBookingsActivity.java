package thegamechanger.club.android.application;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.lucasr.twowayview.TwoWayView;

import java.util.ArrayList;
import java.util.HashMap;

import thegamechanger.club.api.Booker;
import thegamechanger.club.api.support.BookerCallback;
import thegamechanger.club.api.support.Ground;


public class MainBookingsActivity extends Activity implements BookerCallback{
    int gid;
    String requiredCourt;
    int requiredPortions;
    boolean bulk;
    View selected_ground=null;
    View selected_portion=null;
    SimpleAdapter simpleAdapter;

    Booker booker;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_bookings);
        bulk = getIntent().getBooleanExtra("Bulk",false);
        if (Build.VERSION.SDK_INT >= 19) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            if(Build.VERSION.SDK_INT >= 21)
                window.setStatusBarColor(getResources().getColor(R.color.md_green_800));
        }

        booker = ((TheGameChanger)getApplicationContext()).booker;

        ImageButton btn_return = (ImageButton) findViewById(R.id.btn_return);
        btn_return.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        // Prevent keyboard from popping up by default.
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        progressDialog = new ProgressDialog(MainBookingsActivity.this);
        progressDialog.setMessage("Let's see what grounds we got!");
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
        booker.getGrounds(getApplicationContext(), this);
//        searchView = (EditText) findViewById(R.id.grounds_search);
//        searchView.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                simpleAdapter.getFilter().filter(s);
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });
    }

//    private class GetGroundsTask extends AsyncTask<String,String,String> {
//        int sid; boolean bulk;
//        ProgressDialog progressDialog;
//        ArrayList<HashMap<String,String>> groundList = new ArrayList<HashMap<String, String>>();
//        public GetGroundsTask(int sid,boolean bulk) {
//            this.sid = sid;
//            this.bulk = bulk;
//        }
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            progressDialog = new ProgressDialog(MainBookingsActivity.this);
//            progressDialog.setMessage("Let's see what grounds we got!");
//            progressDialog.setIndeterminate(false);
//            progressDialog.setCancelable(false);
//            progressDialog.show();
//        }
//        @Override
//        protected String doInBackground(String... params) {
//            List<NameValuePair> parameters = new ArrayList<NameValuePair>();
//            parameters.add(new BasicNameValuePair("sport",""+sid));
//            parameters.add(new BasicNameValuePair("bulk",bulk?"1":"0"));
//            JsonFetcher jsonFetcher = new JsonFetcher(getApplicationContext(),"/mobile/grounds.php",parameters);
//            JSONObject jsonObject = jsonFetcher.fetch();
//            Log.i("Response",jsonFetcher.getResponseText());
//            SharedPreferences.Editor editor = getSharedPreferences(getString(R.string.sharedPref),MODE_PRIVATE).edit();
//            try {
//                editor.putString("Holidays",jsonObject.getJSONArray("holidays").toString());
//                JSONArray grounds = jsonObject.getJSONArray("grounds");
//                for(int i=0;i<grounds.length();i++) {
//                    JSONObject object = grounds.getJSONObject(i);
//                    HashMap<String,String> map = new HashMap<String,String>();
//                    map.put("data",object.toString());
//                    map.put("gname",object.getString("gname"));
//                    groundList.add(map);
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//                finish();
//            }
//            return null;
//        }
//        @Override
//        protected void onPostExecute(String s) {
//            super.onPostExecute(s);
//        }
//    }

    @Override
    public void dataFetched() {
        ArrayList<HashMap<String,String>> groundList = new ArrayList<>();
        for(Ground ground : booker.grounds) {
            HashMap<String, String> groundMap = new HashMap<>();
            groundMap.put("ground", ground.ground);
            groundMap.put("groundID", ""+ground.groundID);
            groundList.add(groundMap);
        }
        simpleAdapter = new SimpleAdapter(
                getApplicationContext(),
                groundList,
                R.layout.list_row,
                new String[] {"ground","groundID"},
                new int[] {R.id.ground_name,R.id.ground_data}
        );
        ((ListView)findViewById(R.id.grounds_view)).setAdapter(simpleAdapter);
        ListView grounds_view = (ListView) findViewById(R.id.grounds_view);
        grounds_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (selected_ground != null) {
                    selected_ground.setBackgroundColor(Color.parseColor("#00000000"));
                    ((TextView) selected_ground.findViewById(R.id.ground_name)).setTextColor(Color.parseColor("#333333"));
                    selected_ground.setActivated(false);
                }
                view.setActivated(true);
                view.setBackgroundColor(Color.parseColor("#0072bc"));
                selected_ground = view;
                ((TextView) view.findViewById(R.id.ground_name)).setTextColor(Color.parseColor("#FFFFFF"));
                booker.setGround(Integer.parseInt((((TextView) view.findViewById(R.id.ground_data)).getText().toString())));
                activateGroundPicker();
            }
        });
        progressDialog.dismiss();
    }

    @Override
    public void dataFetchFailed() {
        finish();
    }

    void activateGroundPicker() {
        findViewById(R.id.players_prompt).setVisibility(View.VISIBLE);
        findViewById(R.id.portions_view).setVisibility(View.VISIBLE);
        HashMap<Integer, String> portionTitles = new HashMap<>();
        portionTitles.put(16, "Full ground");
        portionTitles.put(8, "Half ground");
        portionTitles.put(4, "Quarter ground");
        ArrayList<HashMap<String,String>> portionsList = new ArrayList<>();
        for(int i = booker.getMinPortions(); i<=16; i*=2) {
            HashMap<String,String> map = new HashMap<>();
            map.put("portion",portionTitles.get(i));
            map.put("portions", i + "");
            portionsList.add(map);
        }
        SimpleAdapter portionsAdapter = new SimpleAdapter(
                getApplicationContext(),
                portionsList,
                R.layout.portions_item,
                new String[] {"portion", "portions"},
                new int[] {R.id.ground_name, R.id.ground_data}
        );

        ((TwoWayView) findViewById(R.id.portions_view)).setAdapter(portionsAdapter);
        ((TwoWayView) findViewById(R.id.portions_view)).setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    if (selected_portion!=null)
                    {
                        selected_portion.setBackgroundResource(R.color.md_green_500);
                    }
                    selected_portion = view.findViewById(R.id.ground_name);
                    selected_portion.setBackgroundColor(Color.parseColor("#0072bc"));
                    requiredPortions = Integer.parseInt(((TextView)view.findViewById(R.id.ground_data)).getText().toString());
                    requiredCourt = ((TextView)selected_portion).getText().toString();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
//            ((ListView)findViewById(R.id.portions_view)).setAdapter(new SimpleAdapter(
//                    getApplicationContext(),
//                    portionsList,
//                    R.layout.list_row,
//                    new String[] {"portion","portions"},
//                    new int[] {R.id.ground_name,R.id.ground_data}
//            ));
//            ((ListView)findViewById(R.id.portions_view)).setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                @Override
//                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                    try {
//                        requiredPortions = Integer.parseInt(((TextView)view.findViewById(R.id.ground_data)).getText().toString());
//                        requiredCourt = ((TextView)view.findViewById(R.id.ground_name)).getText().toString();
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//            });
    }

    public void continue_booking(View view)
    {
        if(selected_portion!=null) {
            Intent new_intent = new Intent(getApplicationContext(), TimePicker.class);
            booker.setGround(gid);
            booker.ground.requiredPortions = requiredPortions;
            startActivity(new_intent);
        } else {
            Toast.makeText(getApplicationContext(), "Please pick a ground and size.", Toast.LENGTH_LONG).show();
        }
    }
}
