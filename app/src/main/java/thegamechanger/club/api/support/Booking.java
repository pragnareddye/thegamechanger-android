package thegamechanger.club.api.support;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Iterator;

/**
 * Created by Kenneth on 28-02-2016.
 *
 * Stores details of a booking, holds status as well as payment authentications.
 */
public class Booking {

    public String txnID;
    public String ticketID;
    public String sport;
    public String ground;
    public String portions;
    public String timing;
    public String days;
    public float subTotal;
    public String coupon;
    public float total;
    public boolean fail = false;

    public String token;
    public String postData;
    public String action;

    public Booking(String jsonData) throws JSONException{
        getData(jsonData);
    }

    public Booking(JSONObject jsonObject) throws JSONException {
        getData(jsonObject);
    }

    public void getData(String jsonData) throws JSONException {
        JSONObject bookingObject = new JSONObject(jsonData);
        JSONObject paymentObject = bookingObject.getJSONObject("payment");
        action = paymentObject.getString("action");
        paymentObject = paymentObject.getJSONObject("post_data");
        Iterator<String> keys = paymentObject.keys();
        postData = "";
        try {
            while (keys.hasNext()) {
                String key = keys.next();
                postData += ("&" + key + "=" + URLEncoder.encode(paymentObject.getString(key), "UTF-8"));
            }
            postData = postData.substring(1);
        } catch (UnsupportedEncodingException e) {
            throw new JSONException(e.getMessage());
        }
        token = bookingObject.getString("token");
    }

    public void getData(JSONObject bookingObject) throws JSONException {
        ticketID = bookingObject.getString("ticket_id");
        sport = bookingObject.getString("sport");
        ground = bookingObject.getString("ground");
        portions = bookingObject.getString("portions");
        timing = bookingObject.getString("timing");
        days = bookingObject.getString("days");
        total = (float)bookingObject.getDouble("total");
        if(bookingObject.has("coupon"))
            coupon = bookingObject.getString("coupon");
        if(bookingObject.has("subtotal"))
            subTotal = (float)bookingObject.getDouble("subtotal");
        if(bookingObject.has("txnid"))
            txnID = bookingObject.getString("txnid");
    }
}
