package thegamechanger.club.api.support;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by Kenneth on 28-02-2016.
 *
 * Stores details of a ground including current status and user's ground requirements.
 */
public class Ground {
    public int groundID;
    public String ground;
    public int portions;
    public int minPortions;
    public float duration;

    public int requiredPortions;
    public long requiredTiming;
    public HashMap<String, Float> cost;
    public long timing;
    public long booked;
    public long reserved;
    public ArrayList<GregorianCalendar> dates;

    public Ground(JSONObject jsonObject) throws JSONException, ParseException {
        getData(jsonObject);
    }

    public void getData(String jsonData) throws JSONException, ParseException {
        JSONObject jsonObject = new JSONObject(jsonData);
        getData(jsonObject);
    }

    public void getData(JSONObject jsonObject) throws JSONException, ParseException {
        if(jsonObject.has("id")) {
            groundID = jsonObject.getInt("id");
            ground = jsonObject.getString("ground");
            portions = jsonObject.getInt("portions");
            minPortions = jsonObject.getInt("min_portions");
            duration = Float.parseFloat(jsonObject.getString("duration"));
        }
        if(jsonObject.has("cost")) {
            JSONObject temp = jsonObject.getJSONObject("cost");
            Iterator<String> keys = temp.keys();
            cost = new HashMap<>();
            while(keys.hasNext()) {
                String key = keys.next();
                cost.put(key, (float)temp.getDouble(key));
            }
        }
        if(jsonObject.has("timing")) {
            timing = jsonObject.getLong("timing");
            if(jsonObject.has("booked")) {
                booked = jsonObject.getLong("booked");
                reserved = jsonObject.getLong("reserved");
            } else {
                booked = reserved = 0;
            }
        }
        if(jsonObject.has("days")) {
            JSONArray days=  jsonObject.getJSONArray("days");
            dates = new ArrayList<>();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            for (int i = 0; i < days.length(); i++) {
                GregorianCalendar date = new GregorianCalendar();
                date.setTime(df.parse(days.getString(i)));
                dates.add(date);
            }
        }
    }
}
