package thegamechanger.club.api;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import thegamechanger.club.android.application.R;
import thegamechanger.club.api.support.BookerCallback;
import thegamechanger.club.api.support.Booking;
import thegamechanger.club.api.support.TokenRefreshThread;

/**
 * Created by Kenneth on 03-03-2016.
 *
 * Class to handle all the user management related internet calls and responses.
 */
public class User {

    public static String token; // JWT storage for app-wide access
    public static String baseURL = "http://api.thegamechanger.club"; // Base URL to be accessed app-wide

    public String name;
    public String nickname;
    public String email;
    public String mobile;
    public String password;
    public String oldPassword;
    public GregorianCalendar dob;
    public ArrayList<Booking> bookings;

    public void login(final Context applicationContext, final BookerCallback activity) {
        String url = baseURL + "/api/authenticate";
        StringRequest loginRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    getData(response);
                    activity.dataFetched();
                } catch (JSONException e) {
                    e.printStackTrace();
                    activity.dataFetchFailed();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                String errorMessage = Booker.processVolleyError(error);
                Toast.makeText(applicationContext, errorMessage, Toast.LENGTH_SHORT).show();
                activity.dataFetchFailed();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                SharedPreferences preferences = applicationContext.getSharedPreferences(applicationContext.getString(R.string.sharedPref), Context.MODE_PRIVATE);
                String email = preferences.getString("Username", "");
                String password = preferences.getString("Password","");
                params.put("email", email);
                params.put("password", password);
                return params;
            }
        };
        loginRequest.setShouldCache(false);
        Volley.newRequestQueue(applicationContext).add(loginRequest);
    }

    public void register(final Context applicationContext, final BookerCallback activity) {
        String url = baseURL + "/api/register";

        StringRequest registerRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                activity.dataFetched();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                String errorMessage = Booker.processVolleyError(error);
                Toast.makeText(applicationContext, errorMessage, Toast.LENGTH_LONG).show();
                activity.dataFetchFailed();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("name", name);
                params.put("nickname", nickname);
                params.put("email", email);
                params.put("mobile", mobile);
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                params.put("dob", df.format(dob.getTime()));
                params.put("password", password);
                return params;
            }
        };
        registerRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        Volley.newRequestQueue(applicationContext).add(registerRequest);
    }

    public void updateProfile(final Context applicationContext, final BookerCallback activity) {
        String url = baseURL + "/api/updateme?token="+token;

        StringRequest updateRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                activity.dataFetched();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                String errorMessage = Booker.processVolleyError(error);
                Toast.makeText(applicationContext, errorMessage, Toast.LENGTH_SHORT).show();
                if(errorMessage.equals("Token expired")) {
                    new TokenRefreshThread(applicationContext, activity) {
                        public void reCall() {
                            updateProfile(applicationContext, activity);
                        }
                    }.start();
                    return;
                }
                activity.dataFetchFailed();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("name", name);
                params.put("nickname", nickname);
                params.put("mobile", mobile);
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                params.put("dob", df.format(dob.getTime()));
                if(oldPassword!=null) {
                    params.put("password", password);
                    params.put("oldpassword", oldPassword);
                }
                return params;
            }
        };
        updateRequest.setShouldCache(false);
        Volley.newRequestQueue(applicationContext).add(updateRequest);
    }

    public void getBookings(final Context applicationContext, final BookerCallback activity) {
        String url = baseURL + "/api/bookings?token="+token;

        StringRequest bookingsRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONArray bookingsArray = new JSONArray(response);
                    bookings = new ArrayList<>();
                    for(int i=0; i<bookingsArray.length(); i++) {
                        bookings.add(new Booking(bookingsArray.getJSONObject(i)));
                    }
                    activity.dataFetched();
                } catch (JSONException e) {
                    e.printStackTrace();
                    activity.dataFetchFailed();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                String errorMessage = Booker.processVolleyError(error);
                Toast.makeText(applicationContext, errorMessage, Toast.LENGTH_SHORT).show();
                if(errorMessage.equals("Token expired")) {
                    new TokenRefreshThread(applicationContext, activity) {
                        public void reCall() {
                            getBookings(applicationContext, activity);
                        }
                    }.start();
                    return;
                }
                activity.dataFetchFailed();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                return  new HashMap<>();
            }
        };
        Volley.newRequestQueue(applicationContext).add(bookingsRequest);
    }

    public void forgotPassword(final Context applicationContext, final BookerCallback activity) {
        String url = baseURL + "/api/forgotpassword";

        StringRequest forgotRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    String message = new JSONObject(response).getString("message");
                    Toast.makeText(applicationContext, message, Toast.LENGTH_LONG).show();
                    activity.dataFetched();
                } catch (JSONException e) {
                    e.printStackTrace();
                    activity.dataFetchFailed();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                String errorMessage = Booker.processVolleyError(error);
                Toast.makeText(applicationContext, errorMessage, Toast.LENGTH_SHORT).show();
                activity.dataFetchFailed();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("email", email);
                return params;
            }
        };
        forgotRequest.setShouldCache(false);
        forgotRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        Volley.newRequestQueue(applicationContext).add(forgotRequest);
    }

    protected void getData(String jsonData) throws JSONException {
        JSONObject authenticateObject = new JSONObject(jsonData);
        token = authenticateObject.getString("token");
        JSONObject meObject = authenticateObject.getJSONObject("me");
        name = meObject.getString("name");
        nickname = meObject.getString("nickname");
        email = meObject.getString("email");
        mobile = meObject.getString("mobile");
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        try {
            dob = new GregorianCalendar();
            dob.setTime(df.parse(meObject.getString("dob")));
        } catch (ParseException e) {
            throw new JSONException(e.getMessage());
        }
    }
}
