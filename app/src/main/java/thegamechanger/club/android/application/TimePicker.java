package thegamechanger.club.android.application;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;

import thegamechanger.club.api.Booker;
import thegamechanger.club.api.support.BookerCallback;


public class TimePicker extends Activity implements DatePickerDialog.OnDateSetListener, BookerCallback {
    float totalPrice = 0,day,night;
    String booked_slots = "", waiting_slots = "";
    ArrayList<String> selectedTimes = new ArrayList<>();
    HashMap<String, Long> timingMap = new HashMap<>();
    long selectedTimesEncoded = 0;

    Booker booker;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_picker);

        if (Build.VERSION.SDK_INT >= 19) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            if(Build.VERSION.SDK_INT >= 21)
                window.setStatusBarColor(getResources().getColor(R.color.md_green_800));
        }

        booker = ((TheGameChanger)getApplicationContext()).booker;

        ImageButton btn_return = (ImageButton) findViewById(R.id.btn_return);
        btn_return.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        progressDialog = new ProgressDialog(TimePicker.this);
        progressDialog.setMessage("Fetching time slots...");
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.show();

        if (booker.multiplebooking) {
            (findViewById(R.id.estimated_cost)).setVisibility(View.INVISIBLE);
            (findViewById(R.id.estimated_cost_value)).setVisibility(View.INVISIBLE);
            (findViewById(R.id.legend)).setVisibility(View.INVISIBLE);
            booker.getTimes(getApplicationContext(), this);
        } else {
            ((TextView)findViewById(R.id.estimated_cost_value)).setText(String.format(Locale.ENGLISH,getString(R.string.currency_symbol)+"%.0f", totalPrice));
            Calendar now = Calendar.getInstance();
            DatePickerDialog dpd = DatePickerDialog.newInstance(
                    TimePicker.this,
                    now.get(Calendar.YEAR),
                    now.get(Calendar.MONTH),
                    now.get(Calendar.DAY_OF_MONTH)
            );
            dpd.setMinDate(now);
            dpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    finish();
                }
            });
            dpd.show(getFragmentManager(), "Datepickerdialog");
        }
    }

    @Override
    public void dataFetched() {
        if(!booker.multiplebooking) {
            day = booker.ground.cost.get("day");
            night = booker.ground.cost.get("night");
        }
        displayTimeSlots();
        progressDialog.dismiss();
    }

    @Override
    public void dataFetchFailed() {
        finish();
    }

    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int year, int monthOfYear, int dayOfMonth) {
        booker.days = new ArrayList<>();
        booker.days.add(new GregorianCalendar(year, monthOfYear, dayOfMonth));
        booker.getTimes(getApplicationContext(), this);
    }

    public void displayTimeSlots() {
        waiting_slots = "";
        booked_slots = "";
        ((TextView) findViewById(R.id.interval)).setText(String.format(Locale.ENGLISH, "Each slot is %d minutes", (int)(booker.ground.duration*60)));
        View.OnClickListener buttonAction = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button button = (Button) v;
                String selectedTime = button.getText().toString();
                DateFormat df = new SimpleDateFormat("hh:mm a");
                float cost = 0;
                try {
                    cost = (df.parse(selectedTime).getTime()-df.parse("06:00 AM").getTime()>=0&&df.parse(selectedTime).getTime()-df.parse("06:00 PM").getTime()<0)?day:night;
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (selectedTimes.contains(selectedTime))
                {
                    selectedTimes.remove(selectedTime);
                    selectedTimesEncoded -= timingMap.get(selectedTime);
                    button.setBackgroundResource(R.drawable.un_checked);
                    totalPrice -= cost;
                }
                else
                {
                    selectedTimes.add(selectedTime);
                    selectedTimesEncoded += timingMap.get(selectedTime);
                    button.setBackgroundResource(R.drawable.checked);
                    totalPrice +=cost;
                }
                ((TextView)findViewById(R.id.estimated_cost_value)).setText(String.format(Locale.ENGLISH,getString(R.string.currency_symbol)+"%.0f", totalPrice));
            }
        };
        //code to display time slots
        DateFormat dF = new SimpleDateFormat("hh:mm aa");
        ArrayList<String> time_slots = new ArrayList<>();
        long timing = booker.ground.timing, booked = booker.ground.booked, reserved = booker.ground.reserved;
        // Check if no slots are available for the day
        if(timing == 0) {
            Toast.makeText(getApplicationContext(), "There are no available slots for this day", Toast.LENGTH_LONG).show();
            finish();
        }
        long mask = 1;
        try {
            for (int i = 0; i < Math.ceil(24.0 / booker.ground.duration); i++,mask=(long)Math.pow(2, i)) {
                if ((mask & timing) == 0)
                    continue;
                String time_string = dF.format(new Date(dF.parse("12:00 AM").getTime() + (long) Math.ceil(i * 3600000 * booker.ground.duration)));
                time_string = time_string.toUpperCase();
                time_slots.add(time_string);
                timingMap.put(time_string, mask);
                if((mask & booked) != 0) {
                    booked_slots += (time_string + ",");
                }
                if((mask & reserved) != 0) {
                    waiting_slots += (time_string + ",");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        int no_of_slots =time_slots.size();
        GridLayout time_slots_view = (GridLayout) findViewById(R.id.time_slots_view);
        int max_slot;
        time_slots_view.setRowCount(4);
        max_slot = 4;
        if (no_of_slots%max_slot==0)
        {
            time_slots_view.setRowCount(no_of_slots / max_slot);
        }
        else
        {
            time_slots_view.setRowCount((no_of_slots / max_slot) + 1);
        }
        int x =0;
        int y = 0;
        for (int i = 0; i < no_of_slots; i++)
        {
            GridLayout.Spec row = GridLayout.spec(y, 1);
            GridLayout.Spec col = GridLayout.spec(x, 1);
            GridLayout.LayoutParams gridParams = new GridLayout.LayoutParams(row, col);
            gridParams.setMargins(8, 8, 8, 8);
            Button button = new Button(this);
            button.setText(time_slots.get(i).toUpperCase());
            button.setTextColor(getResources().getColor(R.color.mdtp_white));
            button.setPadding(10,10,10,10);
            if(booked_slots.contains(button.getText().toString())) {
                button.setBackgroundResource(R.drawable.booked);
            } else if(waiting_slots.contains(button.getText().toString())) {
                button.setBackgroundResource(R.drawable.waiting);
            } else {
                button.setBackgroundResource(R.drawable.un_checked);
                button.setOnClickListener(buttonAction);
            }
            time_slots_view.addView(button, gridParams);
            if (y<(max_slot-1))
            {
                y++;
            }
            else
            {
                y = 0;
                x++;
            }
        }
    }

    public void time_picker_continue_booking(View view)
    {
        Intent intent = new Intent(getApplicationContext(), Confirmation.class);
        if (booker.multiplebooking)
            intent = new Intent(getApplicationContext(), MultipleBookings.class);
        booker.ground.requiredTiming = selectedTimesEncoded;
        if(Long.bitCount(selectedTimesEncoded)<2) {
            Toast.makeText(getApplicationContext(), "Please pick at least two slots.", Toast.LENGTH_LONG).show();
            return;
        }
        System.out.println("Timing Encoded: "+selectedTimesEncoded+" : "+waiting_slots+booker.ground.reserved);
        startActivity(intent);
    }
}
