package thegamechanger.club.android.application;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import thegamechanger.club.api.User;
import thegamechanger.club.api.support.BookerCallback;


public class SplashScreen extends Activity implements BookerCallback {

    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        user = ((TheGameChanger)getApplicationContext()).user;
        if (getSharedPreferences(getString(R.string.sharedPref),MODE_PRIVATE).contains("Username")) {
            user.login(getApplicationContext(), this);
        }
        else {
            Intent i = new Intent(getApplicationContext(),Login.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        }
    }

    @Override
    public void dataFetched() {
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
    }

    @Override
    public void dataFetchFailed() {
        Intent i = new Intent(getApplicationContext(),Login.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
    }
}

