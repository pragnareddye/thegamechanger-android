package thegamechanger.club.api.support;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import thegamechanger.club.api.User;

/**
 * Created by Kenneth on 18-03-2016.
 * Defined to re-obtain token on token expiry and reattempt the fetch
 */
public abstract class TokenRefreshThread extends Thread {

    public Context applicationContext;
    public BookerCallback activity;

    public abstract void reCall();

    public TokenRefreshThread(Context applicationContext, BookerCallback activity) {
        this.applicationContext = applicationContext;
        this.activity = activity;
    }

    @Override
    public void run() {
        super.run();
        RequestFuture<JSONObject> future = RequestFuture.newFuture();
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, User.baseURL + "/api/reauthenticate?token=" + User.token, new JSONObject(), future, future);
        request.setRetryPolicy(new DefaultRetryPolicy(
                15000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        Volley.newRequestQueue(applicationContext).add(request);
        try {
            JSONObject response = future.get(20, TimeUnit.SECONDS);
            User.token = response.getString("token");
            reCall();
            ((Activity)activity).runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(applicationContext, "Token refreshed successfully", Toast.LENGTH_LONG).show();
                }
            });
        } catch (InterruptedException e) {
            e.printStackTrace();
            activity.dataFetchFailed();
        } catch(TimeoutException e) {
            e.printStackTrace();
            System.out.println("Connection timed-out");
            activity.dataFetchFailed();
        } catch(ExecutionException e) {
            e.printStackTrace();
            System.out.println("Fetch failed");
            activity.dataFetchFailed();
        } catch(JSONException e) {
            e.printStackTrace();
            System.out.println("Invalid server response");
            activity.dataFetchFailed();
        } catch(ClassCastException e) {
            e.printStackTrace();
            System.out.println("Unable to show toast refresh");
        }
    }
}
