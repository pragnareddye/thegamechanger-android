package thegamechanger.club.android.application;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageButton;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.accountswitcher.AccountHeader;
import com.mikepenz.materialdrawer.accountswitcher.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileSettingDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;


public class About extends Activity {

    AccountHeader headerResult;
    Drawer result;
    String name, email, img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        if (Build.VERSION.SDK_INT >= 19) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            if (Build.VERSION.SDK_INT >= 21)
                window.setStatusBarColor(getResources().getColor(R.color.md_green_800));
        }

        //Back button functionality
        ImageButton btn_return = (ImageButton) findViewById(R.id.btn_return);
        btn_return.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(i);
                finish();
            }
        });

        //Navigation Drawer
        SharedPreferences preferences = getSharedPreferences(getString(R.string.sharedPref), MODE_PRIVATE);
        name = preferences.getString("Name", "");
        email = preferences.getString("Username", "");
        img = preferences.getString("Image", null);

        Drawable profileDrawable = null;
        if(profileDrawable==null) profileDrawable = getResources().getDrawable(R.drawable.default_icon);
        headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.drawable.bg5)
                .addProfiles(
                        new ProfileDrawerItem().withName(name).withEmail(email).withIcon(getResources().getDrawable(R.drawable.tgc_white)),
                        new ProfileSettingDrawerItem().withName("Profile").withDescription("View User Profile")
                )
                .build();

        result = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(new Toolbar(this))
                .withAccountHeader(headerResult)
                .withActionBarDrawerToggle(true)
                .withActionBarDrawerToggleAnimated(true)
                .withTranslucentStatusBar(false)
                .addDrawerItems(
                        new PrimaryDrawerItem().withName("Home").withTag("Home"),
                        new PrimaryDrawerItem().withName("Profile").withTag("Profile"),
                        new DividerDrawerItem(),
                        new PrimaryDrawerItem().withName("About").withTag("About"),
                        new PrimaryDrawerItem().withName("Log Out").withTag("Log Out")
                        //new SecondaryDrawerItem().withName("Settings").withTag("Settings")
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(AdapterView<?> parent, View view, int position, long id, IDrawerItem drawerItem) {
                        Intent intent;
                        switch(drawerItem.getTag().toString())
                        {
                            case "Home":
                                intent = new Intent(getApplicationContext(), MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                //result.setSelection(0);
                                break;
                            case "Profile":
                                intent = new Intent(getApplicationContext(), Profile.class);
                                startActivity(intent);
                                //result.setSelection(1);
                                break;

                            case "Log Out":
                                intent = new Intent(getApplicationContext(), Login.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                break;

                        }
                        return true;
                    }
                })
                .build();
        result.setSelection(3);
    }

}
