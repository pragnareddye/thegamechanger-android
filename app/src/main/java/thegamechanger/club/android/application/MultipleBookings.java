package thegamechanger.club.android.application;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import thegamechanger.club.api.Booker;
import thegamechanger.club.api.support.BookerCallback;


public class MultipleBookings extends Activity implements DatePickerDialog.OnDateSetListener, BookerCallback{
    private ArrayList<HashMap<String, String>> dateList = new ArrayList<>();
    private SimpleAdapter dateAdapter;
    private ListView dateList_view;
    private DatePickerDialog datePickerDialog;
    private float totalPrice = 0;

    Booker booker;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multiple_bookings);

        if (Build.VERSION.SDK_INT >= 19) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            if(Build.VERSION.SDK_INT >= 21)
                window.setStatusBarColor(getResources().getColor(R.color.md_green_800));
        }
        ImageButton btn_return = (ImageButton) findViewById(R.id.btn_return);
        btn_return.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        booker = ((TheGameChanger)getApplicationContext()).booker;

        progressDialog = new ProgressDialog(MultipleBookings.this);
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Fetching available dates...");
        progressDialog.show();

        booker.getDays(getApplicationContext(), this);
        booker.days = new ArrayList<>();

        dateAdapter = new SimpleAdapter(
                getApplicationContext(),
                dateList,
                R.layout.date_list_row,
                new String[]{"date","cost"},
                new int[]{R.id.selected_date,R.id.date_data});
        dateList_view = (ListView)findViewById(R.id.date_time_list);
        dateList_view.setAdapter(dateAdapter);

        ((ListView)findViewById(R.id.date_time_list)).setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                totalPrice -= Float.parseFloat(((TextView)view.findViewById(R.id.date_data)).getText().toString());
                dateList.remove(position);
                booker.days.remove(position);
                ((BaseAdapter) dateList_view.getAdapter()).notifyDataSetChanged();
                ((TextView)findViewById(R.id.estimated_cost)).setText(String.format(Locale.ENGLISH,getString(R.string.currency_symbol)+"%.0f", totalPrice));
            }
        });
    }

    public void addNewDate(View view)
    {
        datePickerDialog.show(getFragmentManager(), "Datepickerdialog");
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        HashMap<String,String> map = new HashMap<>();
        String date = dayOfMonth+"/"+(monthOfYear+1)+"/"+year;
        boolean exists = false;
        for (HashMap<String, String> i : dateList) {
            Iterator obj_i = i.entrySet().iterator();
            Map.Entry pair = (Map.Entry)obj_i.next();
            Log.d("PairValue", pair.getValue().toString());
            if (pair.getValue().toString().equals(date))
            {
                Toast.makeText(getApplicationContext(),"You have already picked that date. Please choose another.", Toast.LENGTH_SHORT).show();
                exists = true;
                break;
            }
        }
        if(!exists) {
            map.put("date", date);
            Calendar cal = Calendar.getInstance();
            try {
                cal.setTime(new SimpleDateFormat("d/M/y").parse(date));
                int dow = cal.get(Calendar.DAY_OF_WEEK);
                float cost;
                if(dow == Calendar.SUNDAY||dow == Calendar.SATURDAY||dow == Calendar.FRIDAY) {
                    cost = booker.ground.cost.get("weekend");
                } else {
                    cost = booker.ground.cost.get("weekday");
                }
                map.put("cost",""+cost);
                totalPrice+=cost;
            } catch (Exception e) {
                e.printStackTrace();
            }
            dateList.add(map);
            booker.days.add(new GregorianCalendar(year,monthOfYear, dayOfMonth));
            dateAdapter.notifyDataSetChanged();
            ((TextView)findViewById(R.id.estimated_cost)).setText(String.format(Locale.ENGLISH,getString(R.string.currency_symbol)+"%.0f",totalPrice));
        }
    }

    public void multiple_continue_booking(View view)
    {
        if (booker.days.size() == 0)
            Toast.makeText(getApplicationContext(), "Please pick a date.", Toast.LENGTH_SHORT).show();
        else
            startActivity(new Intent(getApplicationContext(), Confirmation.class));
    }

    @Override
    public void dataFetched() {
        Calendar[] calendars = new Calendar[booker.ground.dates.size()];
        int counter=0;
        for(GregorianCalendar date : booker.ground.dates) {
            calendars[counter] = Calendar.getInstance();
            calendars[counter].setTime(date.getTime());
            counter++;
        }
        Calendar now = calendars[0];
        datePickerDialog = DatePickerDialog.newInstance(
                MultipleBookings.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        datePickerDialog.setMinDate(now);
        datePickerDialog.setSelectableDays(calendars);
        //datePickerDialog.setHighlightedDays(waitingCalendars);
        progressDialog.dismiss();
    }

    @Override
    public void dataFetchFailed() {
        finish();
    }
}
