package thegamechanger.club.api.support;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Kenneth on 28-02-2016.
 *
 * Holds details of a confirmed reservation and to be applied coupon.
 */
public class Reserve {
    public String ticketID;
    public String sport;
    public String ground;
    public String portions;
    public String timing;
    public String days;
    public float subTotal;
    public String coupon = null;
    public float discount;
    public float total;

    public Reserve(String jsonData) throws JSONException{
        JSONObject jsonObject = new JSONObject(jsonData);
        ticketID = jsonObject.getString("ticket_id");
        sport = jsonObject.getString("sport");
        ground = jsonObject.getString("ground");
        portions = jsonObject.getString("portions");
        timing = jsonObject.getString("timing");
        days = jsonObject.getString("days");
        subTotal = (float)jsonObject.getDouble("subtotal");
    }

    public void getData(String jsonData) throws JSONException {
        JSONObject couponObject = new JSONObject(jsonData);
        discount = (float)couponObject.getDouble("discount");
        total = (float)couponObject.getDouble("total");
    }
}
