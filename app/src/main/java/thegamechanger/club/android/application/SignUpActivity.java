package thegamechanger.club.android.application;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.GregorianCalendar;
import java.util.Locale;

import thegamechanger.club.api.User;
import thegamechanger.club.api.support.BookerCallback;


public class SignUpActivity extends Activity implements BookerCallback, DatePickerDialog.OnDateSetListener {

    User user;
    private DatePickerDialog datePickerDialog;
    private int day = 1, month = 1, year = 1970;
    public ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        if (Build.VERSION.SDK_INT>=19)
        {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            if(Build.VERSION.SDK_INT >= 21)
                window.setStatusBarColor(getResources().getColor(R.color.md_green_800));
        }

        user = new User();

        datePickerDialog = DatePickerDialog.newInstance(
                SignUpActivity.this,
                year,
                month,
                day
        );

        ImageButton btn_return = (ImageButton) findViewById(R.id.btn_return);
        btn_return.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        findViewById(R.id.btn_register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fname = ((EditText) findViewById(R.id.first_name)).getText().toString();
                String nickname = ((EditText) findViewById(R.id.nickname)).getText().toString();
                String email = ((EditText) findViewById(R.id.e_mail)).getText().toString();
                String mobile = ((EditText) findViewById(R.id.mobile_no)).getText().toString();
                String password = ((EditText) findViewById(R.id.new_password)).getText().toString();
                String re_password = ((EditText) findViewById(R.id.confirm_password)).getText().toString();
                if (password.equals(re_password)) {
                    progressDialog = new ProgressDialog(SignUpActivity.this);
                    progressDialog.setMessage("Registering you for greatness...");
                    progressDialog.setIndeterminate(false);
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    user.name = fname;
                    user.nickname = nickname;
                    user.email = email;
                    user.password = password;
                    user.mobile = mobile;
                    user.dob = new GregorianCalendar(year, month, day);
                    user.register(getApplicationContext(), SignUpActivity.this);
                } else {
                    Toast.makeText(getApplicationContext(), "Password mismatch", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public void dataFetched() {
        progressDialog.dismiss();
        Toast.makeText(getApplicationContext(), "Registered, please activate your email", Toast.LENGTH_SHORT).show();
        startActivity(new Intent(getApplicationContext(), Login.class));
    }

    @Override
    public void dataFetchFailed() {
        progressDialog.dismiss();
    }

    public void setDate(View view){
        datePickerDialog.show(getFragmentManager(), "Datepickerdialog");
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        day = dayOfMonth;
        month = monthOfYear;
        this.year = year;
        ((TextView) findViewById(R.id.date_of_birth)).setText(String.format(Locale.ENGLISH, "%d/%d/%d", day, (month+1), this.year));
    }
}
