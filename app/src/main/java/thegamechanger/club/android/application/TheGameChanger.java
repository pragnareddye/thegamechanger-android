package thegamechanger.club.android.application;

import android.app.Application;

import thegamechanger.club.api.Booker;
import thegamechanger.club.api.User;

/**
 * Created by Kenneth on 01-08-2015.
 */
public class TheGameChanger extends Application {
    public Booker booker = new Booker();
    public User user = new User();
}