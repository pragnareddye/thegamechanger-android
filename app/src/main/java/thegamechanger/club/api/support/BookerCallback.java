package thegamechanger.club.api.support;

/**
 * Created by Kenneth on 01-03-2016.
 *
 * Interface to expose methods of the activity for notifying successful completion or failure of a network request.
 */
public interface BookerCallback {

    void dataFetched();
    void dataFetchFailed();
}
