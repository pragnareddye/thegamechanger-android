package thegamechanger.club.api.support;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Kenneth on 28-02-2016.
 *
 * Facilitates storage of list of sports and associated details.
 */
public class Sport {
    public int sportID;
    public String sport;
    public int minPortions;

    public Sport(int id, String name, int min_portions) {
        sportID = id;
        sport = name;
        minPortions = min_portions;
    }

    public Sport(String jsonData) throws JSONException {
        getData(jsonData);
    }

    public Sport(JSONObject sportObject) throws JSONException{
        getData(sportObject);
    }

    public void getData(String jsonData) throws JSONException {
        JSONObject sportObject = new JSONObject(jsonData);
        getData(sportObject);
    }

    public void getData(JSONObject sportObject) throws JSONException {
        sportID = sportObject.getInt("id");
        sport = sportObject.getString("sport");
        minPortions = Integer.parseInt(sportObject.getString("min_portions"));
    }
}
