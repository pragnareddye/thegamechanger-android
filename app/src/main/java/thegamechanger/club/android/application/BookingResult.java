package thegamechanger.club.android.application;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Locale;

import thegamechanger.club.api.Booker;

public class BookingResult extends Activity {

    Booker booker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_result);

        booker = ((TheGameChanger)getApplicationContext()).booker;

        if (Build.VERSION.SDK_INT >= 19) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            if(Build.VERSION.SDK_INT >= 21 && booker.booking.fail)
                window.setStatusBarColor(getResources().getColor(R.color.md_red_900));
            else if(Build.VERSION.SDK_INT >= 21)
                window.setStatusBarColor(getResources().getColor(R.color.md_green_800));
        }
        ImageButton btn_return = (ImageButton) findViewById(R.id.btn_return);
        btn_return.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        if(booker.booking.fail) {
            findViewById(R.id.booking_result_title_bar).setBackgroundColor(getResources().getColor(R.color.md_red_700));
            ((TextView) findViewById(R.id.activity_title)).setText(R.string.booking_failed);
            ((TextView)findViewById(R.id.booking_status)).setText(R.string.payment_failed);
            ((ImageView)findViewById(R.id.booking_status_img)).setImageResource(R.drawable.fail);
            ((Button)findViewById(R.id.button)).setText(R.string.retry);
            findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        } else {
            ((TextView) findViewById(R.id.activity_title)).setText(R.string.ticket_details);
            findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getApplicationContext(), Profile.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
                }
            });
        }

        ((TextView)findViewById(R.id.transaction_id)).setText(String.format(getResources().getString(R.string.transaction_id_prompt), booker.booking.txnID));
        ((TextView)findViewById(R.id.ticket_id)).setText(String.format(getResources().getString(R.string.ticket_id_prompt), booker.booking.ticketID));
        ((TextView)findViewById(R.id.sport)).setText(booker.booking.sport);
        ((TextView)findViewById(R.id.size)).setText(booker.booking.portions);
        ((TextView)findViewById(R.id.dates)).setText(booker.booking.days);
        ((TextView)findViewById(R.id.time_slots)).setText(booker.booking.timing);
        ((TextView)findViewById(R.id.total_price)).setText(String.format(Locale.ENGLISH,getString(R.string.currency_symbol)+"%.0f", booker.booking.subTotal));
        ((TextView)findViewById(R.id.discount_coupon)).setText(booker.booking.coupon);
        ((TextView)findViewById(R.id.discount)).setText(String.format(Locale.ENGLISH,getString(R.string.currency_symbol)+"%.0f", (booker.booking.subTotal - booker.booking.total)));
        ((TextView)findViewById(R.id.sub_total)).setText(String.format(Locale.ENGLISH,getString(R.string.currency_symbol)+"%.0f", booker.booking.total));
    }

    @Override
    public void onBackPressed()
    {
        startActivity(new Intent(getApplicationContext(), MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
    }

}
