package thegamechanger.club.android.application;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.accountswitcher.AccountHeader;
import com.mikepenz.materialdrawer.accountswitcher.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileSettingDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import java.util.Locale;

import thegamechanger.club.api.Booker;
import thegamechanger.club.api.User;


public class MainActivity extends Activity {

    int selectedSport= 0;
    ImageView selectedImage;
    SectionsPagerAdapter mSectionsPagerAdapter;
    ViewPager mViewPager;
    AccountHeader headerResult;
    Drawer result;
    TextView singleBookingHint;
    TextView multipleBookingHint;
    User user;
    Booker booker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (Build.VERSION.SDK_INT >= 19) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            if (Build.VERSION.SDK_INT >= 21)
                window.setStatusBarColor(getResources().getColor(R.color.md_green_800));
        }

        user = ((TheGameChanger)getApplicationContext()).user;
        booker = ((TheGameChanger)getApplicationContext()).booker;
        booker.getSports();

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        //Retrieve username through SharedPreferences
        SharedPreferences preferences = getSharedPreferences(getString(R.string.sharedPref), MODE_PRIVATE);
        if (!preferences.contains("NotFirstTime"))
        {
            preferences.edit().putBoolean("NotFirstTime", true).apply();
            findViewById(R.id.tutorial).setVisibility(View.VISIBLE);
        }
        headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.drawable.bg5)
                .addProfiles(
                        new ProfileDrawerItem().withName(user.name).withEmail(user.email).withIcon(getResources().getDrawable(R.drawable.tgc_white)),
                        new ProfileSettingDrawerItem().withName("Profile").withDescription("View User Profile")
                )
                .build();

        result = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(new Toolbar(this))
                .withAccountHeader(headerResult)
                .withActionBarDrawerToggle(true)
                .withActionBarDrawerToggleAnimated(true)
                .withTranslucentStatusBar(false)
                .addDrawerItems(
                        new PrimaryDrawerItem().withName("Home").withTag("Home"),
                        new PrimaryDrawerItem().withName("Profile").withTag("Profile"),
                        new DividerDrawerItem(),
                        new PrimaryDrawerItem().withName("About").withTag("About"),
                        new PrimaryDrawerItem().withName("Log Out").withTag("Log Out")
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(AdapterView<?> parent, View view, int position, long id, IDrawerItem drawerItem) {
                        Intent intent;
                        switch (drawerItem.getTag().toString()) {
                            case "Profile":
                                intent = new Intent(getApplicationContext(), Profile.class);
                                startActivityForResult(intent, 0);
                                break;

                            case "About":
                                intent = new Intent(getApplicationContext(), About.class);
                                startActivityForResult(intent, 0);
                                break;

                            case "Log Out":
                                intent = new Intent(getApplicationContext(), Login.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                break;

                        }
                        return true;
                    }
                })
                .build();
        result.setSelection(0);
        //Add listener for navigation drawer
        ImageButton nav_menu = (ImageButton) findViewById(R.id.btn_show_nav);
        nav_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                result.openDrawer();
            }
        });
    }

    public void start_booking(View view)
    {
        boolean bulk = false;
        RadioGroup rdgrpBookingType = (RadioGroup) findViewById(R.id.booking_type);
        switch(rdgrpBookingType.getCheckedRadioButtonId())
        {
            case R.id.single_booking:
                break;
            case R.id.multiple_booking:
                bulk = true;
                break;
        }
        if (rdgrpBookingType.getCheckedRadioButtonId()==-1 || selectedSport == 0) {
            Toast.makeText(getApplicationContext(), "Select a sport and booking type", Toast.LENGTH_SHORT).show();
        } else {
            Intent i = new Intent(getApplicationContext(), MainBookingsActivity.class);
            switch(selectedSport)
            {
                case R.id.football:
                    booker.setSport(1);
                    break;
                case R.id.volleyball:
                    booker.setSport(2);
                    break;
                case R.id.frisbee:
                    booker.setSport(4);
                    break;
                case R.id.rugby:
                    booker.setSport(8);
                    break;
            }
            booker.multiplebooking = bulk;
            startActivity(i);
        }
    }

    public void selectSport(View view)
    {
        RelativeLayout selectedLayout;
        if (selectedSport!=0)
        {
            Log.d("Previous ID", ""+selectedSport);
            selectedImage.setImageAlpha(255);
        }
        selectedSport = view.getId();
        selectedLayout = (RelativeLayout) findViewById(selectedSport);
        selectedImage = (ImageView) selectedLayout.getChildAt(0);
        selectedImage.setImageAlpha(128);
    }

    /**
     * A {@link android.support.v13.app.FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            switch (position) {
                case 0:
                    return BookingsFragment.newInstance(position + 1);
            }
            return null;
        }

        @Override
        public int getCount() {
            return 1;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return getString(R.string.title_bookings).toUpperCase(l);
            }
            return null;
        }
    }

    public static class BookingsFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static BookingsFragment newInstance(int sectionNumber) {
            BookingsFragment fragment = new BookingsFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public BookingsFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            return inflater.inflate(R.layout.fragment_bookings, container, false);
        }
    }

    public void booking_hint(View view){
        singleBookingHint= (TextView) findViewById(R.id.single_booking_hint);
        multipleBookingHint = (TextView) findViewById(R.id.multiple_booking_hint);
        switch(view.getId())
        {
            case R.id.single_booking:
                singleBookingHint.setVisibility(View.VISIBLE);
                multipleBookingHint.setVisibility(View.INVISIBLE);
                break;
            case R.id.multiple_booking:
                multipleBookingHint.setVisibility(View.VISIBLE);
                singleBookingHint.setVisibility(View.INVISIBLE);
                break;
        }
    }
    public void remove_tutorial(View view) {
        view.setVisibility(View.GONE);
    }

    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data){
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        finish();
        startActivity(i);
    }
}
