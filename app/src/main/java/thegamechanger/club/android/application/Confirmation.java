package thegamechanger.club.android.application;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

import thegamechanger.club.api.Booker;
import thegamechanger.club.api.support.BookerCallback;


public class Confirmation extends Activity implements BookerCallback{

    float totalPrice = 0, discount = 0;

    Booker booker;
    ProgressDialog progressDialog;
    Toast toast = null;
    boolean confirmed = false;
    boolean applyingCoupon = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmation);
        if (Build.VERSION.SDK_INT >= 19) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            if(Build.VERSION.SDK_INT >= 21)
                window.setStatusBarColor(getResources().getColor(R.color.md_green_800));
        }

        booker = ((TheGameChanger)getApplicationContext()).booker;

        ImageButton btn_return = (ImageButton) findViewById(R.id.btn_return);
        btn_return.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        progressDialog = new ProgressDialog(Confirmation.this);
        progressDialog.setMessage("Confirming...");
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.show();

        findViewById(R.id.apply).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog = new ProgressDialog(Confirmation.this);
                progressDialog.setMessage("Applying discount...");
                progressDialog.setIndeterminate(false);
                progressDialog.setCancelable(false);
                progressDialog.show();
                applyingCoupon = true;
                booker.reserve.coupon = ((EditText) findViewById(R.id.editText)).getText().toString();
                booker.applyCoupon(getApplicationContext(), Confirmation.this);
            }
        });

        findViewById(R.id.proceed_to_pay_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), PaymentPortal.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
            }
        });

        if(!confirmed) {
            booker.makeReservation(getApplicationContext(), this);
            confirmed = !confirmed;
        } else
            dataFetched();
    }

    @Override
    public void dataFetched() {
        if(!applyingCoupon) {
            ((TextView)findViewById(R.id.ground)).setText(booker.reserve.ground);
            ((TextView) findViewById(R.id.size)).setText(booker.reserve.portions);
            ((TextView) findViewById(R.id.sport)).setText(booker.reserve.sport);
            ((TextView) findViewById(R.id.dates)).setText(booker.reserve.days);
            ((TextView) findViewById(R.id.time_slots)).setText(booker.reserve.timing);
            totalPrice = booker.reserve.subTotal;
            ((TextView) findViewById(R.id.total_price)).setText(String.format(Locale.ENGLISH,getString(R.string.currency_symbol)+"%.0f",totalPrice));
            ((TextView) findViewById(R.id.sub_total)).setText(String.format(Locale.ENGLISH,getString(R.string.currency_symbol)+"%.0f",totalPrice));
        } else {
            applyingCoupon = false;
            discount = booker.reserve.discount;
            ((TextView)findViewById(R.id.discount)).setText(String.format(Locale.ENGLISH,getString(R.string.currency_symbol)+"%.0f",discount));
            ((TextView)findViewById(R.id.sub_total)).setText(String.format(Locale.ENGLISH,getString(R.string.currency_symbol)+"%.0f",(totalPrice-discount)));
        }
        progressDialog.dismiss();
    }

    @Override
    public void dataFetchFailed() {
        if (!applyingCoupon)
            finish();
        else {
            applyingCoupon = false;
            booker.reserve.coupon = null;
            ((EditText) findViewById(R.id.editText)).setText("");
            progressDialog.dismiss();
        }
    }

    @Override
    public void onBackPressed()
    {
        if (toast == null || toast.getView().getWindowVisibility() != View.VISIBLE) {
            toast = Toast.makeText(getApplicationContext(), getString(R.string.booking_cancel_confirmation), Toast.LENGTH_SHORT);
            toast.show();
        } else {
            toast.cancel();
            startActivity(new Intent(getApplicationContext(), MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
        }
    }
}