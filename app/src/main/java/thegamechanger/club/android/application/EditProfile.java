package thegamechanger.club.android.application;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

import thegamechanger.club.api.User;
import thegamechanger.club.api.support.BookerCallback;


public class EditProfile extends Activity implements BookerCallback, DatePickerDialog.OnDateSetListener {

    User user, newUser;
    ProgressDialog progressDialog;
    private DatePickerDialog datePickerDialog;
    private int day = 1, month = 1, year = 1970;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        if (Build.VERSION.SDK_INT>=19)
        {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            if(Build.VERSION.SDK_INT >= 21)
                window.setStatusBarColor(getResources().getColor(R.color.md_green_800));
        }

        user = ((TheGameChanger)getApplicationContext()).user;
        newUser = new User();
        newUser.email = user.email;
        newUser.oldPassword = null;
        if (user.dob != null) {
            day = user.dob.get(Calendar.DAY_OF_MONTH);
            month = user.dob.get(Calendar.MONTH);
            year = user.dob.get(Calendar.YEAR);
            ((TextView) findViewById(R.id.date_of_birth)).setText(String.format(Locale.ENGLISH, "%d/%d/%d", day, (month+1), this.year));
        }

        datePickerDialog = DatePickerDialog.newInstance(
                EditProfile.this,
                year,
                month,
                day
        );

        ImageButton btn_return = (ImageButton) findViewById(R.id.btn_return);
        btn_return.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ((TextView) findViewById(R.id.title)).setText(R.string.edit_profile);
        ((TextView)findViewById(R.id.e_mail_prompt)).setText(R.string.old_password_prompt);
        ((EditText)findViewById(R.id.e_mail)).setHint("Old password");
        ((EditText) findViewById(R.id.first_name)).setText(user.name);
        ((EditText) findViewById(R.id.nickname)).setText(user.nickname);
        ((EditText) findViewById(R.id.mobile_no)).setText(user.mobile);
        ((Button)findViewById(R.id.btn_register)).setText(R.string.update);
        findViewById(R.id.btn_register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newUser.name = ((EditText) findViewById(R.id.first_name)).getText().toString();
                newUser.nickname = ((EditText) findViewById(R.id.nickname)).getText().toString();
                String oldPassword = ((EditText) findViewById(R.id.e_mail)).getText().toString();
                newUser.mobile = ((EditText) findViewById(R.id.mobile_no)).getText().toString();
                newUser.dob = new GregorianCalendar(year, month, day);
                String password = ((EditText) findViewById(R.id.new_password)).getText().toString();
                String re_password = ((EditText) findViewById(R.id.confirm_password)).getText().toString();
                if (password.equals(re_password)) {
                    if(!oldPassword.equals("")) {
                        newUser.oldPassword = oldPassword;
                        newUser.password = password;
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Password mismatch", Toast.LENGTH_LONG).show();
                }

                progressDialog = new ProgressDialog(EditProfile.this);
                progressDialog.setIndeterminate(false);
                progressDialog.setCancelable(false);
                progressDialog.setMessage("Updating details...");
                progressDialog.show();
                newUser.updateProfile(getApplicationContext(), EditProfile.this);
            }
        });
    }

    @Override
    public void dataFetched() {
        progressDialog.dismiss();
        ((TheGameChanger)getApplicationContext()).user = newUser;
        finish();
        //startActivity(new Intent(getApplicationContext(), Profile.class));
    }

    @Override
    public void dataFetchFailed() {
        progressDialog.dismiss();
    }

    public void setDate(View view){
        datePickerDialog.show(getFragmentManager(), "Datepickerdialog");
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        day = dayOfMonth;
        month = monthOfYear;
        this.year = year;
        ((TextView) findViewById(R.id.date_of_birth)).setText(String.format(Locale.ENGLISH, "%d/%d/%d", day, (month + 1), this.year));
    }
}
