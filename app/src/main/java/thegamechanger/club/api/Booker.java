package thegamechanger.club.api;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import thegamechanger.club.api.support.BookerCallback;
import thegamechanger.club.api.support.Booking;
import thegamechanger.club.api.support.Ground;
import thegamechanger.club.api.support.Reserve;
import thegamechanger.club.api.support.Sport;
import thegamechanger.club.api.support.TokenRefreshThread;

/**
 * Created by Kenneth on 28-02-2016.
 *
 * Class to handle all the booking related internet calls and responses.
 */
public class Booker {

    public ArrayList<Sport> sports;
    public ArrayList<Ground> grounds;
    public boolean multiplebooking;
    public Sport sport;
    public Ground ground;
    public ArrayList<GregorianCalendar> days = new ArrayList<>();
    public Reserve reserve;
    public Booking booking;

    public void getSports() {
        sports = new ArrayList<>();
        sports.add(new Sport(1, "Football", 4));
        sports.add(new Sport(2, "Volley ball", 4));
        sports.add(new Sport(4, "Frisbee", 16));
        sports.add(new Sport(8, "Rugby", 16));
    }

    public void setSport(int sportID) {
        for (Sport sport : sports) {
            if(sport.sportID == sportID) {
                this.sport = sport;
                return;
            }
        }
    }

    public void getGrounds(final Context applicationContext, final BookerCallback activity) {
        String url = User.baseURL + "/api/grounds?token="+User.token;
        grounds = new ArrayList<>();

        StringRequest groundsRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    getGrounds(response);
                    activity.dataFetched();
                } catch (JSONException e) {
                    e.printStackTrace();
                    activity.dataFetchFailed();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                String errorMessage = processVolleyError(error);
                Toast.makeText(applicationContext, errorMessage, Toast.LENGTH_SHORT).show();
                if(errorMessage.equals("Token expired")) {
                    new TokenRefreshThread(applicationContext, activity) {
                        public void reCall() {
                            getGrounds(applicationContext, activity);
                        }
                    }.start();
                    return;
                }
                activity.dataFetchFailed();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("sport", ""+sport.sportID);
                return params;
            }
        };
        groundsRequest.setRetryPolicy(new DefaultRetryPolicy(
                15000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        Volley.newRequestQueue(applicationContext).add(groundsRequest);
    }

    public void getGrounds(String jsonData) throws JSONException {
        JSONArray jsonGrounds = new JSONArray(jsonData);
        getGrounds(jsonGrounds);
    }

    public void getGrounds(JSONArray jsonGrounds) throws JSONException {
        try {
            for(int i=0; i<jsonGrounds.length(); i++)
                grounds.add(new Ground(jsonGrounds.getJSONObject(i)));
        } catch(ParseException e) {
            e.printStackTrace();
            throw new JSONException("Date parse error");
        }
    }

    public void setGround(int groundID) {
        for (Ground ground : grounds) {
            if(ground.groundID == groundID) {
                this.ground = ground;
                return;
            }
        }
    }

    public int getMinPortions() {
        return Math.max(sport.minPortions, ground.minPortions);
    }

    public void getTimes(final Context applicationContext, final BookerCallback activity) {
        String url = User.baseURL + "/api/times?token="+User.token;

        StringRequest timesRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    ground.getData(response);
                    activity.dataFetched();
                } catch (Exception e) {
                    e.printStackTrace();
                    activity.dataFetchFailed();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                String errorMessage = processVolleyError(error);
                Toast.makeText(applicationContext, errorMessage, Toast.LENGTH_SHORT).show();
                if(errorMessage.equals("Token expired")) {
                    new TokenRefreshThread(applicationContext, activity) {
                        public void reCall() {
                            getTimes(applicationContext, activity);
                        }
                    }.start();
                    return;
                }
                activity.dataFetchFailed();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("ground", ""+ground.groundID);
                if(!multiplebooking) {
                    params.put("portions", ""+ground.requiredPortions);
                    SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
                    params.put("date", ft.format(days.get(0).getTime()));
                }
                return params;
            }
        };
        timesRequest.setShouldCache(false);
        timesRequest.setRetryPolicy(new DefaultRetryPolicy(
                15000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        Volley.newRequestQueue(applicationContext).add(timesRequest);
    }

    public void getDays(final Context applicationContext, final BookerCallback activity) {
        String url = User.baseURL + "/api/days?token="+User.token;

        StringRequest daysRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    ground.getData(response);
                    activity.dataFetched();
                } catch (Exception e) {
                    e.printStackTrace();
                    activity.dataFetchFailed();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                String errorMessage = processVolleyError(error);
                Toast.makeText(applicationContext, errorMessage, Toast.LENGTH_SHORT).show();
                if(errorMessage.equals("Token expired")) {
                    new TokenRefreshThread(applicationContext, activity) {
                        public void reCall() {
                            getDays(applicationContext, activity);
                        }
                    }.start();
                    return;
                }
                activity.dataFetchFailed();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("ground", ""+ground.groundID);
                params.put("portions", "" + ground.requiredPortions);
                params.put("timing", ""+ground.requiredTiming);
                return params;
            }
        };
        daysRequest.setShouldCache(false);
        daysRequest.setRetryPolicy(new DefaultRetryPolicy(
                15000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        Volley.newRequestQueue(applicationContext).add(daysRequest);
    }

    public void makeReservation(final Context applicationContext, final BookerCallback activity) {
        String url = User.baseURL + "/api/reserve?token="+User.token;

        StringRequest reserveRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    reserve = new Reserve(response);
                    activity.dataFetched();
                } catch (JSONException e) {
                    e.printStackTrace();
                    activity.dataFetchFailed();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                String errorMessage = processVolleyError(error);
                Toast.makeText(applicationContext, errorMessage, Toast.LENGTH_SHORT).show();
                if(errorMessage.equals("Token expired")) {
                    new TokenRefreshThread(applicationContext, activity) {
                        public void reCall() {
                            makeReservation(applicationContext, activity);
                        }
                    }.start();
                    return;
                }
                activity.dataFetchFailed();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("sport", ""+sport.sportID);
                params.put("ground", ""+ground.groundID);
                params.put("portions", ""+ground.requiredPortions);
                params.put("timing", ""+ground.requiredTiming);
                String dateString = "";
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                for(GregorianCalendar day : days) {
                    dateString+=(df.format(day.getTime())+",");
                }
                dateString = dateString.substring(0, dateString.length()-1);
                System.out.println("Date Encoded: " + dateString);
                params.put("days", dateString);
                return params;
            }
        };
        reserveRequest.setShouldCache(false);
        reserveRequest.setRetryPolicy(new DefaultRetryPolicy(
                15000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        Volley.newRequestQueue(applicationContext).add(reserveRequest);
    }

    public void applyCoupon(final Context applicationContext, final BookerCallback activity) {
        String url = User.baseURL + "/api/coupon?token="+User.token;

        StringRequest couponRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    reserve.getData(response);
                    activity.dataFetched();
                } catch (JSONException e) {
                    e.printStackTrace();
                    activity.dataFetchFailed();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                String errorMessage = processVolleyError(error);
                Toast.makeText(applicationContext, errorMessage, Toast.LENGTH_SHORT).show();
                if(errorMessage.equals("Token expired")) {
                    new TokenRefreshThread(applicationContext, activity) {
                        public void reCall() {
                            applyCoupon(applicationContext, activity);
                        }
                    }.start();
                    return;
                }
                activity.dataFetchFailed();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("coupon", reserve.coupon);
                params.put("ticket_id", reserve.ticketID);
                return params;
            }
        };
        couponRequest.setShouldCache(false);
        couponRequest.setRetryPolicy(new DefaultRetryPolicy(
                15000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        Volley.newRequestQueue(applicationContext).add(couponRequest);
    }

    public void paymentDetails(final Context applicationContext, final BookerCallback activity) {
        String url = User.baseURL + "/api/pay?token="+User.token;

        StringRequest paymentRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    booking = new Booking(response);
                    User.token = booking.token;
                    activity.dataFetched();
                } catch (JSONException e) {
                    e.printStackTrace();
                    activity.dataFetchFailed();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                String errorMessage = processVolleyError(error);
                Toast.makeText(applicationContext, errorMessage, Toast.LENGTH_SHORT).show();
                if(errorMessage.equals("Token expired")) {
                    new TokenRefreshThread(applicationContext, activity) {
                        public void reCall() {
                            paymentDetails(applicationContext, activity);
                        }
                    }.start();
                    return;
                }
                activity.dataFetchFailed();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                if(reserve.discount>0)
                    params.put("coupon", reserve.coupon);
                params.put("ticket_id", reserve.ticketID);
                return params;
            }
        };
        paymentRequest.setShouldCache(false);
        paymentRequest.setRetryPolicy(new DefaultRetryPolicy(
                15000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        Volley.newRequestQueue(applicationContext).add(paymentRequest);
    }

    public static String processVolleyError(VolleyError error) {
        String errorMessage = "Unknown error";
        String statusCode = "Unknown code";
        try {
            //get status code here
            statusCode = String.valueOf(error.networkResponse.statusCode);
            //get response body and parse with appropriate encoding
            if (error.networkResponse.data != null) {
                try {
                    String body = new String(error.networkResponse.data, "UTF-8");
                    System.out.println(body);
                    JSONObject errorJSON = new JSONObject(body);
                    if (errorJSON.has("error"))
                        errorMessage = errorJSON.getString("error");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    errorMessage = "Server Malfunction";
                } catch (JSONException e) {
                    e.printStackTrace();
                    errorMessage = "Server error";
                }
            } else {
                errorMessage = "Internet error";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e("Booker/User", statusCode + ": " + errorMessage);
        errorMessage = errorMessage.replace("_"," ");
        errorMessage = Character.toUpperCase(errorMessage.charAt(0)) + errorMessage.substring(1);
        return errorMessage;
    }
}
