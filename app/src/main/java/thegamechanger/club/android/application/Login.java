package thegamechanger.club.android.application;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import thegamechanger.club.api.User;
import thegamechanger.club.api.support.BookerCallback;


public class Login extends Activity implements BookerCallback {
    final Context context = this;

    @Override
    public void onConfigurationChanged(Configuration newConfig) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        SharedPreferences preferences = getSharedPreferences(getString(R.string.sharedPref),MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove("Username");
        editor.remove("Password");
        editor.apply();
        View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validate(v);
            }
        };
        findViewById(R.id.txt_forgot_password).setOnClickListener(clickListener);
        findViewById(R.id.txt_new_user).setOnClickListener(clickListener);
        findViewById(R.id.btn_log_in).setOnClickListener(clickListener);
    }

    public void validate(View view) {
        if (view.getId()==R.id.btn_log_in) {
            EditText txtUsername = (EditText) findViewById(R.id.username);
            EditText txtPassword = (EditText) findViewById(R.id.password);
            String username = txtUsername.getText().toString();
            String password = txtPassword.getText().toString();
            SharedPreferences preferences = getSharedPreferences(getString(R.string.sharedPref),MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("Username",username);
            editor.putString("Password",password);
            editor.apply();
            startActivity(new Intent(getApplicationContext(),SplashScreen.class));
        }
        else if (view.getId()==R.id.txt_new_user)
        {
            Intent i = new Intent(getApplicationContext(), SignUpActivity.class);
            startActivity(i);
        }
        else if (view.getId()==R.id.txt_forgot_password)
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            LayoutInflater inflater = getLayoutInflater();
            final View inflator = inflater.inflate(R.layout.forgot_password, null);
            builder.setMessage(R.string.forgot_password_prompt)
                    .setView(inflator)
                    .setTitle(R.string.forgot_password);
            final EditText emailEditText = (EditText)inflator.findViewById(R.id.email_forgot_password);
            builder.setPositiveButton("Recover", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String email = emailEditText.getText().toString();
                    dialog.dismiss();
                    User user = new User();
                    user.email = email;
                    user.forgotPassword(getApplicationContext(), Login.this);
                }
            })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //Do nothing
                        }
                    });
            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    @Override
    public void dataFetched() {
    }

    @Override
    public void dataFetchFailed() {

    }
}
