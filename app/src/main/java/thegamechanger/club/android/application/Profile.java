package thegamechanger.club.android.application;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.accountswitcher.AccountHeader;
import com.mikepenz.materialdrawer.accountswitcher.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileSettingDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import java.util.ArrayList;
import java.util.HashMap;

import thegamechanger.club.api.User;
import thegamechanger.club.api.support.BookerCallback;
import thegamechanger.club.api.support.Booking;


public class Profile extends Activity implements BookerCallback {
    AccountHeader headerResult;
    SimpleAdapter simpleAdapter;
    ArrayList<HashMap<String,String>> bookingList;

    User user;
    View header;
    ListView list;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        if (Build.VERSION.SDK_INT>=19)
        {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            if (Build.VERSION.SDK_INT>=21)
                window.setStatusBarColor(getResources().getColor(R.color.md_green_800));
        }

        header = getLayoutInflater().inflate(R.layout.activity_profile_header, null);
        list = (ListView)findViewById(R.id.bookingsList);
        list.addHeaderView(header);

        user = ((TheGameChanger)getApplicationContext()).user;
        progressDialog = new ProgressDialog(Profile.this);
        progressDialog.setMessage("Looking you up...");
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
        user.getBookings(getApplicationContext(), this);

        ImageButton btn_return = (ImageButton) findViewById(R.id.btn_return);
        btn_return.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ((TextView)findViewById(R.id.name)).setText(user.name);
        ((TextView)findViewById(R.id.e_mail)).setText(user.email);
        ((TextView)findViewById(R.id.mobile_no)).setText(user.mobile);

        //Navigation Drawer
        headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.drawable.bg5)
                .addProfiles(
                        new ProfileDrawerItem().withName(user.name).withEmail(user.email).withIcon(getResources().getDrawable(R.drawable.tgc_white)),
                        new ProfileSettingDrawerItem().withName("Profile").withDescription("View User Profile")
                )
                .build();

        Drawer result = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(new Toolbar(this))
                .withAccountHeader(headerResult)
                .withActionBarDrawerToggle(true)
                .withActionBarDrawerToggleAnimated(true)
                .withTranslucentStatusBar(false)
                .addDrawerItems(
                        new PrimaryDrawerItem().withName("Home").withTag("Home"),
                        new PrimaryDrawerItem().withName("Profile").withTag("Profile"),
                        new DividerDrawerItem(),
                        new PrimaryDrawerItem().withName("About").withTag("About"),
                        new PrimaryDrawerItem().withName("Log Out").withTag("Log Out")
                        //new SecondaryDrawerItem().withName("Settings").withTag("Settings")
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(AdapterView<?> parent, View view, int position, long id, IDrawerItem drawerItem) {
                        Intent intent;
                        switch (drawerItem.getTag().toString()) {
                            case "Home":
                                intent = new Intent(getApplicationContext(), MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                break;
                            case "About":
                                intent = new Intent(getApplicationContext(), About.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                break;
                            case "Log Out":
                                intent = new Intent(getApplicationContext(), Login.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                break;
                        }
                        return true;
                    }
                })
                .build();
        result.setSelection(1);
    }

    public void edit_profile(View view)
    {
        Intent intent = new Intent(getApplicationContext(), EditProfile.class);
        startActivityForResult(intent, 0);
    }

    @Override
    public void dataFetched() {
        bookingList = new ArrayList<>();
        for(Booking booking : user.bookings) {
            HashMap<String,String> bookingMap = new HashMap<>();
            bookingMap.put("ticket_id", booking.ticketID);
            bookingMap.put("sport", booking.sport);
            bookingMap.put("ground", booking.ground);
            bookingMap.put("portions", booking.portions);
            bookingMap.put("timing", booking.timing);
            bookingMap.put("days", booking.days);
            bookingList.add(bookingMap);
        }
        if(bookingList.size()==0) {
            HashMap<String, String> bookingMap = new HashMap<>();
            bookingMap.put("ticket_id", "");
            bookingMap.put("sport", "");
            bookingMap.put("ground", "No bookings to view");
            bookingMap.put("portions", "");
            bookingMap.put("timing", "");
            bookingMap.put("days", "");
            bookingList.add(bookingMap);
        }
        simpleAdapter = new SimpleAdapter(
                getApplicationContext(),
                bookingList,
                R.layout.bookings_row,
                new String[] {"ticket_id", "ground", "sport", "portions", "timing", "days"},
                new int[] {R.id.booking_id, R.id.ground_name, R.id.sport, R.id.game_type, R.id.booking_type, R.id.date_string}
        );
        list.setAdapter(simpleAdapter);
        progressDialog.dismiss();
    }

    @Override
    public void dataFetchFailed() {
        user.bookings = new ArrayList<>();
        bookingList = new ArrayList<>();
        dataFetched();
    }

    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data){
        Intent i = new Intent(getApplicationContext(), Profile.class);
        finish();
        startActivity(i);
    }
}
