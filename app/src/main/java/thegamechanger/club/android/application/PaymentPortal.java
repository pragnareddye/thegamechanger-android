package thegamechanger.club.android.application;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.Toast;

import org.apache.http.util.EncodingUtils;
import org.json.JSONObject;

import thegamechanger.club.api.Booker;
import thegamechanger.club.api.User;
import thegamechanger.club.api.support.BookerCallback;


public class PaymentPortal extends Activity implements BookerCallback {

    Booker booker;
    ProgressDialog progressDialog;
    Toast toast = null;

    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_portal);
        if (Build.VERSION.SDK_INT>=19)
        {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            if (Build.VERSION.SDK_INT>=21)
                window.setStatusBarColor(getResources().getColor(R.color.md_green_800));
        }

        booker = ((TheGameChanger)getApplicationContext()).booker;

        ImageButton btn_return = (ImageButton) findViewById(R.id.btn_return);
        btn_return.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        webView = (WebView)findViewById(R.id.paymentWebView);

        progressDialog = new ProgressDialog(PaymentPortal.this);
        progressDialog.setMessage("Fetching payment authorization...");
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.show();

        booker.paymentDetails(getApplicationContext(), this);
    }

    @Override
    public void dataFetched() {
        progressDialog.dismiss();
        progressDialog = null;

        Log.i("POSTDATA", booker.booking.postData);

        webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                Log.i("PaymentWebView: ", "SSL error: " + error);
                handler.proceed();
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.i("PaymentWebView: ", "URL Loaded: " + url);
                if (url.equals(User.baseURL + "/api/success")) {
                    Toast.makeText(getApplicationContext(), "Success! " + url,
                            Toast.LENGTH_SHORT).show();
                } else if (url.equals(User.baseURL + "/api/fail")) {
                    Toast.makeText(getApplicationContext(), "Failure! " + url,
                            Toast.LENGTH_SHORT).show();
                }
                return super.shouldOverrideUrlLoading(view, url);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                if (url.equals(User.baseURL + "/api/success")) {
                    Toast.makeText(getApplicationContext(), "Success!", Toast.LENGTH_SHORT).show();
                    String title = webView.getTitle();
                    Log.i("JSONTitle", title);
                    try {
                        booker.booking.getData(new JSONObject(title));
                        startActivity(new Intent(getApplicationContext(), BookingResult.class));
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), "Internal Error", Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(getApplicationContext(), Profile.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(i);
                    }
                } else if (url.equals(User.baseURL + "/api/fail")) {
                    Toast.makeText(getApplicationContext(), "Failure!", Toast.LENGTH_SHORT).show();
                    String title = webView.getTitle();
                    try {
                        booker.booking.getData(new JSONObject(title));
                        booker.booking.fail = true;
                        startActivity(new Intent(getApplicationContext(), BookingResult.class));
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), "Internal Error", Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(getApplicationContext(), Profile.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(i);
                    }
                }
            }
        });
        webView.setWebChromeClient(new WebChromeClient());

        webView.setVisibility(View.VISIBLE);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setCacheMode(2);
        webView.getSettings().setDomStorageEnabled(true);
        webView.clearHistory();
        webView.clearCache(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setUseWideViewPort(false);
        webView.getSettings().setLoadWithOverviewMode(false);

        webView.postUrl(booker.booking.action, EncodingUtils.getBytes(booker.booking.postData, "BASE64"));
    }

    @Override
    public void dataFetchFailed() {
        finish();
    }

    @Override
    public void onBackPressed()
    {
        if (toast == null || toast.getView().getWindowVisibility() != View.VISIBLE) {
            toast = Toast.makeText(getApplicationContext(), getString(R.string.booking_cancel_confirmation), Toast.LENGTH_SHORT);
            toast.show();
        } else {
            toast.cancel();
            startActivity(new Intent(getApplicationContext(), MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
        }
    }
}